# killit-cli
A simple utility to allow interactive killing of processes from CLI in Bash

After seeing an interactive command-line process killer featured on [OMG Ubuntu](http://www.omgubuntu.co.uk/2017/04/gkill-process-killer-interactive-cli) that required the GO framework, I knew that I could create a similar utility utilizing only Bash and [pick](https://github.com/thoughtbot/pick) - which has packages for Debian, Ubuntu, OS X, FreeBSD, and can be compiled from source.

Currently the PS way is activated; uncomment the PGREP method (and comment out the PS way) if that works better on your system.  The PS way should work out of the box on Debian and Ubuntu and derivatives.  My PS is *procps-ng version 3.3.9*.

## Dependencies
* [pick](https://github.com/thoughtbot/pick)
* ps or pgrep
* awk (if you use the PS option)
* grep

Because I call subshells (which have the string we're searching for as part of the command) grep is needed regardless of which method you choose.

## Usage

killit.sh [optional string you want to kill]

If you don't enter a string to search for, it will prompt you for one. 