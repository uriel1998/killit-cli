#!/bin/bash

################################################################################
#  A simple utility to allow interactive killing of processes from CLI
#
#  by Steven Saus
#
################################################################################

	User=$(whoami)
	UProc=$(ps aux | grep -c "steven")

	# check for pick or choose here

	if [ "$1" == "" ]; then	
		echo "Type a string to filter the $UProc processes for $User"
		echo "or a single Q to quit."
		read CHOICE
	else
		CHOICE=$(echo "$1")
	fi
	Choice=$(echo "$CHOICE" | tr '[:upper:]' '[:lower:]')
	
	if [ "$Choice" = "q" ];then
		exit
	fi
	
	# The PS way - it gives us more arguments to the command line 
	Killstring=$(ps aux | grep -e "$Choice" | grep -v "grep" | grep -v "killit" | awk '{print $2"\t"$3"\t"$4"\t"substr($0,index($0,$11))}' | pick)

	# The PGREP way
	# There isn't a way around grep here, since I'm using a subshell, and finding the PID of the subshell would just iterate around. 
	#Killstring=$(pgrep --list-full --full "$Choice" | grep -v "killit" | pick)

	# The PGREP and parents only way
	#Killstring=$(pgrep -P 1 --list-full --full "$Choice" | grep -v "killit" | pick)


	KillProc=$(echo "$Killstring" | awk '{print $1}')
	kill $KillProc